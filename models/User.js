const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  name: String,
  surname: String,
  gender: String,
  birthday: String,
  education: String,
  section: String,
  salary: Number
})

module.exports = mongoose.model('User', userSchema)
