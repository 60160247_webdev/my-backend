const mongoose = require('mongoose')
const User = require('./models/User')

mongoose.connect('mongodb://localhost/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

User.find((err, users) => {
  if (err) return console.error(err)
  console.log(users)
})

// try {
//   const users = await User.find({})
//   res.json(users)
// } catch (err) {
//   res.status(500).send(err)
// }
