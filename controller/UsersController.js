const User = require('../models/User')
const usersController = {
  userList: [
    {
      id: 1,
      name: 'Thanakrit',
      surname: 'Wutthiphithak',
      gender: 'ผู้ชาย',
      birthday: '12 มีนาคม 2542',
      education: 'ปริญญาตรี',
      section: 'แผนกไอที',
      salary: 20000
    },
    {
      id: 2,
      name: 'Yanisa',
      surname: 'Tongtavee',
      gender: 'ผู้หญิง',
      birthday: '27 สิงหาคม 2542',
      education: 'ปริญญาตรี',
      section: 'แผนกการตลาด',
      salary: 30000
    }
  ],
  lastId: 3,
  async addUser (req, res, next) {
    const payload = req.body
    const user = new User(payload)
    try {
      await user.save()
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getUsers (req, res, next) {
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = usersController
